--=======================================================
--======================== Shape ========================
--=======================================================
local Shape = {}
Shape.__index = Shape
function Shape:new (o,color,filled)
   o = o or {}
   setmetatable(o, self)
   self.__index = self
   self.color = color
   self.filled = filled
   return o
end

function Shape:setColor(newColor)
   self.color = newColor
end

function Shape:getColor()
   return self.color
end

function Shape:setFilled(newFilled)
   self.filled = newFilled
end

function Shape:isFilled()
   return self.filled
end

function Shape:print ()
   print("I am a Shape ")
end

--=======================================================
--======================== Rectangle ========================
--=======================================================
local Rectangle = Shape:new()
Rectangle.__index = Rectangle
function Rectangle:new (o,length,width,color,filled)
   o = o or Shape:new(o)
   setmetatable(o, self)
   self.__index = self
   self.length = length
   self.width = width
   self.area = length * width
   self.perimeter = 2 * (length + width)
   return o
end

function Rectangle:setWidth(newWidth)
   self.width = newWidth
end

function Rectangle:getWidth()
   return self.width
end

function Rectangle:setLength(newLength)
   self.length = newLength
end

function Rectangle:getLength()
   return self.length
end

function Rectangle:getArea()
   return self.area
end

function Rectangle:getPerimeter()
   return self.perimeter
end

function Rectangle:print ()
    print("I am a Rectangle and a Shape ")
end

function Rectangle:printCalculation ()
   print("\nThe length of a Rectangle is", self.length, ", and the width of a Rectangle is",self.width)
   print("=> The perimeter of this Rectangle is", self.perimeter)
   print("=> The area of this Rectangle is", self.area)
end

--=======================================================
--======================== Square ========================
--=======================================================
local Square = Shape:new()
Square.__index = Square
function Square:new (o,side,color,filled)
   o = o or Shape:new(o)
   setmetatable(o, self)
   self.__index = self
   self.side = side
   self.area = side*side
   self.perimeter = 4 * side
   return o
end

function Square:setSide(newSide)
   self.side = newSide
end

function Square:getSide()
   return self.side
end

function Square:getArea()
   return self.area
end

function Square:getPerimeter()
   return self.perimeter
end

function Square:print ()
   print("I am a Square, Rectangle , and Shape ")
end

function Square:printCalculation ()
   print("\nThe side of a Square is",self.side)
   print("=> The perimeter of this Square is",self.perimeter)
   print("=> The area of this Square is",self.area)
end

--=======================================================
--======================== Circle ========================
--=======================================================
local Circle = Shape:new()
Circle.__index = Circle
function Circle:new (o,radius,color,filled)
   o = o or Shape:new(o)
   setmetatable(o, self)
   self.__index = self
   self.color = color
   self.filled = filled
   self.radius = radius
   self.area = radius*radius*3.1416
   self.perimeter = 3.1416 * 2 * radius
   return o
end

function Circle:setRadius(newRadius)
   self.radius = newRadius
end

function Circle:getRadius()
   return self.radius
end

function Circle:getArea()
   return self.area
end

function Circle:getPerimeter()
   return self.perimeter
end

function Circle:print ()
   print("I am a Circle and a Shape  ")
end

function Circle:printCalculation ()
   print("\nThe radius of a Circle is",self.radius)
   print("=> The perimeter of this Circle is", self.perimeter)
   print("=> The area of this Circle is", self.area)
end

-- Creating an object
local shape = Shape:new(nil,"red",true)
shape:print()

local circle = Circle:new(nil,10)
circle:print()

local rectangle = Rectangle:new(nil,10,20)
rectangle:print()

local square = Square:new(nil,10)
square:print()

circle:printCalculation()
rectangle:printCalculation()
square:printCalculation()




