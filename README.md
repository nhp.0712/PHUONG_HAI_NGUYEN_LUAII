------------------------------------------------------------------------------------------------------------------
 Implement the equivalent of the Class diagram shown in the image link location below:
 https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
 -----------------------------------------------------------------------------------------------------------------
 You MUST include test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.




                                Example Solutions for all functions


I am a Shape 
I am a Circle and a Shape  
I am a Rectangle and a Shape 
I am a Square, Rectangle , and Shape 

The radius of a Circle is	10
=> The perimeter of this Circle is	62.832
=> The area of this Circle is	314.16

The length of a Rectangle is	10	, and the width of a Rectangle is	20
=> The perimeter of this Rectangle is	60
=> The area of this Rectangle is	200

The side of a Square is	10
=> The perimeter of this Square is	40
=> The area of this Square is	100